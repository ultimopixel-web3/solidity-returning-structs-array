// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract ReturningStructsArray{

    struct Person{
        bytes32 name;
        uint age;
    }

    Person[] public people;

    constructor(){
        people.push(Person({
            name: "Young Person",
            age: 18
        }));
        people.push(Person({
            name: "Adult Person",
            age: 19
        }));
        people.push(Person({
            name: "Methuselah",
            age: 350
        }));
    }

    function getPeople() view external returns(bytes32[] memory, uint[] memory){

        bytes32[] memory names = new bytes32[](people.length);
        uint[] memory ages = new uint[](people.length);

        for(uint i = 0; i < people.length; i++){
            names[i] = people[i].name;
            ages[i] = people[i].age;
        }

        return (names, ages);

    }

}